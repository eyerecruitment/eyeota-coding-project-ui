# Eyeota UI Coding Project

## The challenge

The objective is to build a reusable *table component* using one of the modern frameworks: Ember.js (preferred), 
React, Angular or similar - **do let us know beforehand**

![table](images/table.png)

### Requirements

UI:

* table needs to be sortable (clicking on the header will sort the data by that column) - sorting is done on the backend, 
  it also needs to be able to cancel existing sorting
* pagination support (pagination needs to be async - send an ajax request to the server at `/data` endpoint)
* no additional dependencies apart from standard framework
* use any build tools that you are familiar with (Gulp, Grunt, Webpack, etc)

Server:

* route `/data` will return an array of object in JSON format. This needs to handle pagination on the query 
  (e.g. `/data?start=0&limit=10` should return the first 10 items) 
* route will also handles sorting e.g. `/data?sort=username` will sort by username field
* data is stored in `routes/index.js`, feel free to modify the data and try to store a large amount of data (1k rows)

For both:

* code needs to be tested and *jslint* errors and warnings free

## Project structure

You are free to structure the project as you wish. What we are looking for is your ability to architect the code that 
is clean, easy to understand and testable

# Result

You should send the result in a *.zip* file or *tarball* (created with command `tar -czvf`) file and **should not** push it to the public repo.

# License

Copying, sharing or publishing is not allowed.

Copyright 2014-2017 Eyeota